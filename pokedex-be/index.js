const express = require("express");
const mongoose = require("mongoose");
const routes = require("./routes");
require("dotenv").config();

mongoose
  .connect(process.env.ATLAS_MONGODB_URI || "mongodb://localhost:27017/pokedex", {
    useNewUrlParser: true,
  })
  .then(() => {
    const app = express();
    app.use(express.json());
    app.use(function (req, res, next) {
      res.header(
        "Access-Control-Allow-Origin",
        process.env.URL || "http://localhost:3000"
      );
      res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
      );
      res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
      next();
    });
    app.use("/api", routes);

    const port = process.env.PORT || 5000;

    app.listen(port, () => {
      console.log("Server has started!");
    });
  });
