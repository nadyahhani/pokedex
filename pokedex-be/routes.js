const express = require("express");
const Pokemon = require("./models/Pokemon"); // new
const router = express.Router();

router.get("/pokemons", async (req, res) => {
  const pokemons = await Pokemon.find();
  const count = Object.values(
    pokemons.reduce((a, { name }) => {
      a[name] = a[name] || { name, count: 0 };
      a[name].count++;
      return a;
    }, Object.create(null))
  );
  res.send({
    countTotal: pokemons.length,
    countEach: count,
    pokemons: pokemons,
  });
});

router.post("/pokemons", async (req, res) => {
  try {
    const newpoke = new Pokemon({
      name: req.body.name,
      nickname: req.body.nickname,
      url: req.body.url,
      imgurl: req.body.imgurl,
      pokeid: req.body.pokeid
    });
    await newpoke.save();
    res.send(newpoke);
  } catch {
    res.status(500);
    res.send({ error: "Internal Server Error" });
  }
});

router.delete("/pokemons/delete/:pokeid", async (req, res) => {
  try {
    await Pokemon.deleteOne({ _id: req.params.pokeid });
    res.status(204).send();
  } catch {
    res.status(500);
    res.send({ error: "Internal Server Error" });
  }
});

module.exports = router;
