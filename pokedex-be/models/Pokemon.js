const mongoose = require("mongoose");

const pokemon = mongoose.Schema({
  name: String,
  nickname: String,
  url: String,
  imgurl: String,
  pokeid: String
});

module.exports = mongoose.model("Pokemon", pokemon);
