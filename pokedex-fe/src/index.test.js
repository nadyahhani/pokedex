import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import React from "react";
const { configure, mount, shallow } = require("enzyme");
import Reducer from "./services/Reducer";
import Store, { Context } from "./services/Store";
import App from "./App";
import axios from "axios";
import { act } from "react-test-renderer";

configure({ adapter: new Adapter() });
describe("pokemon editing in reducer", () => {
  const initialState = {
    pokemons: [],
    countTotal: 0,
    countEach: [],
    loading: true,
    notification: {},
  };
  let newState;

  it("should load the initialState", () => {
    expect(Reducer({}, "testing")).toEqual({});
  });

  it("should be able to add a pokemon", () => {
    // check if pokemon is stored locally, api is hit, and new pokemon displayed
    newState = Reducer(initialState, {
      type: "ADD_POKEMON",
      payload: {
        name: "bulbasaur",
        nickname: "boulby",
        url: "https://pokeapi.co/api/v2/pokemon/1/",
        imgurl:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png",
        pokeid: "1",
        _id: "001",
      },
    });
    expect(newState.pokemons).toHaveLength(1);
    expect(newState.countTotal).toBe(1);
    expect(newState.countEach.length).toBe(1);

    // check countTotal and countEach corresponding to the added pokemon
    expect(newState.countEach[0].name).toEqual("bulbasaur");
    expect(newState.countEach[0].count).toEqual(1);
  });

  it("should be able to add the same pokemon", () => {
    // check if pokemon is stored locally, api is hit, and new pokemon displayed
    newState = Reducer(newState, {
      type: "ADD_POKEMON",
      payload: {
        name: "bulbasaur",
        nickname: "boulby 2",
        url: "https://pokeapi.co/api/v2/pokemon/1/",
        imgurl:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png",
        pokeid: "1",
        _id: "002",
      },
    });
    expect(newState.pokemons).toHaveLength(2);
    expect(newState.countTotal).toBe(2);
    expect(newState.countEach.length).toBe(1);

    // check countTotal and countEach corresponding to the added pokemon
    expect(newState.countEach[0].name).toEqual("bulbasaur");
    expect(newState.countEach[0].count).toEqual(2);
  });
  it("deletes from more than 1 to correct amount", () => {
    newState = Reducer(newState, {
      type: "DELETE_POKEMON",
      payload: { name: "bulbasaur", _id: "001" },
    });
    expect(newState.pokemons).toHaveLength(1);
    expect(newState.countTotal).toBe(1);
    expect(newState.countEach.length).toBe(1);

    // check countTotal and countEach corresponding to the added pokemon
    expect(newState.countEach[0].name).toEqual("bulbasaur");
    expect(newState.countEach[0].count).toEqual(1);
  });
  it("deletes pokemon from 1 to 0 correctly", () => {
    // check if local store is changed correctly, api is hit, and displayed properly
    newState = Reducer(newState, {
      type: "DELETE_POKEMON",
      payload: { name: "bulbasaur", _id: "002" },
    });
    expect(newState.pokemons).toHaveLength(0);

    // check countTotal and countEach corresponding to the deleted pokemon
    expect(newState.countTotal).toBe(0);
    expect(newState.countEach.length).toBe(0);
  });
});

// describe("integration of entire application", () => {
//   const wrapper = mount(
//     <Store>
//       <App />
//     </Store>
//   );

//   it("should open poke list when navbar pokedex is clicked", () => {
//     act(() => {
//       wrapper.find("#navbar-pokedex-button").at(0).simulate("click");
//     });
//     expect(window.location.pathname).toEqual("/pokedex/1");
//   });

//   it("should show pokemon details", () => {});
// });
