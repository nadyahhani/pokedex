import { ThemeProvider } from "@material-ui/core";
import { useContext, useEffect } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import LandingPage from "./containers/LandingPage";
import MyPokePage from "./containers/MyPokePage";
import PokeDetailPage from "./containers/PokeDetailPage";
import PokeListPage from "./containers/PokeListPage";
import { theme } from "./global";
import { getMyPokemons } from "./services/pokedexapi";
import { Context } from "./services/Store";

function App() {
  const { dispatch } = useContext(Context);

  useEffect(() => {
    dispatch({ type: "SET_LOADING", payload: true });
    // get all stored / owned pokemon from db
    getMyPokemons((res) => {
      dispatch({ type: "SET_POKEMON", payload: res });
      dispatch({ type: "SET_LOADING", payload: false });
    });
  }, [dispatch]);

  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <Switch>
          <Route exact path="/" component={LandingPage} />
          <Route exact path="/pokedex/:page" component={PokeListPage} />
          <Route exact path="/detail/:pokeid" component={PokeDetailPage} />
          <Route exact path="/my-pokemons" component={MyPokePage} />
          {/* <Route component={ErrorPage} /> */}
        </Switch>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
