const { createMuiTheme } = require("@material-ui/core");

export const theme = createMuiTheme({
  palette: {
    primary: { main: "#CC0000", dark: "#CC0000", light: "#CC0000" },
    secondary: { main: "#fafafa", dark: "#fafafa", light: "#fafafa" },
  },
  typography: {
    fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif"`,
  },

  overrides: {
    MuiBadge: {
      root: {
        display: "block",
      },
      colorSecondary: {
          color: "#CC0000"
      }
    },
    MuiButton: {
      label: {
        textTransform: "none",
        fontWeight: "medium",
      },
      contained: {
        fontWeight: "bold",
      },
      containedPrimary: {
        "& .MuiButton-label": {
          color: "#fafafa",
        },
      },
      containedSecondary: {
        "& .MuiButton-label": {
          color: "#CC0000",
        },
      },
      textSecondary: {
        "& .MuiButton-label": {
          color: "#fafafa",
        },
      },
    },
    MuiGrid: {
      "grid-xs-12": {
        width: "100%",
      },
      "grid-xs-10": {
        width: "83.333333%",
        maxWidth: " 1440px",
      },
      "grid-xs-4": {
        width: "33.333333%",
      },
    },
  },
});
