const Reducer = (state, action) => {
  let temp;
  switch (action.type) {
    case "SET_NOTIF":
      return {
        ...state,
        notification: action.payload,
      };
    case "SET_LOADING":
      return {
        ...state,
        loading: action.payload,
      };
    case "SET_POKEMON":
      return {
        ...state,
        pokemons: action.payload.pokemons,
        countTotal: action.payload.countTotal,
        countEach: action.payload.countEach,
      };
    case "ADD_POKEMON":
      temp = state.countEach.filter((item, idx) => {
        return item.name === action.payload.name;
      });
      return {
        ...state,
        pokemons: state.pokemons.concat(action.payload),
        countTotal: state.countTotal + 1,
        countEach: state.countEach
          .filter((item) => {
            return item.name !== action.payload.name;
          })
          .concat({
            name: action.payload.name,
            count: temp.length < 1 ? 1 : temp[0].count + 1,
          }),
      };
    case "DELETE_POKEMON":
      temp = state.countEach.filter((item, idx) => {
        return item.name === action.payload.name;
      });
      const other = state.countEach.filter((item) => {
        return item.name !== action.payload.name;
      });
      return {
        ...state,
        pokemons: state.pokemons.filter(
          (poke) => poke._id !== action.payload._id
        ),
        countTotal: state.countTotal - 1,
        countEach:
          other.length < 1 && temp[0].count <= 1
            ? []
            : [
                ...other,
                temp[0].count > 1
                  ? {
                      name: action.payload.name,
                      count: temp[0].count - 1,
                    }
                  : null,
              ],
      };
    default:
      return state;
  }
};

export default Reducer;
