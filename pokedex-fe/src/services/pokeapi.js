import axios from "axios";

export function getPokeList(afterFunc, page = 1) {
  const limit = 30;
  const offset = (page - 1) * limit;
  axios
    .get(`https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=${limit}`)
    .then(
      (response) => {
        afterFunc({
          ...response.data,
        });
      },
      (error) => {}
    )
    .catch((err) => {});
}

export function getPokeDetail(afterFunc, id) {
  axios
    .get(`https://pokeapi.co/api/v2/pokemon/${id}`)
    .then(
      (response) => {
        afterFunc(response.data);
      },
      (error) => {}
    )
    .catch((err) => {});
}
