import axios from "axios";

const url =
  process.env.NODE_ENV === "development"
    ? "http://localhost:5000/api/pokemons"
    : "https://pokedex-be.herokuapp.com/api/pokemons";

export function getMyPokemons(afterFunc) {
  axios
    .get(`${url}`)
    .then(
      (response) => {
        afterFunc(response.data);
      },
      (error) => {
      }
    )
    .catch((err) => {
    });
}

export function storePokemon(afterFunc, caughtData) {
  axios
    .post(`${url}`, caughtData)
    .then(
      (response) => {
        afterFunc(response.data);
      },
      (error) => {}
    )
    .catch((err) => {});
}

export function deletePokemon(afterFunc, pokeId) {
  axios
    .delete(`${url}/delete/${pokeId}`)
    .then(
      (response) => {
        afterFunc(response.data);
      },
      (error) => {}
    )
    .catch((err) => {});
}
