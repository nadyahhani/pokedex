import {
  AppBar,
  Chip,
  Grid,
  Paper,
  Toolbar,
  Typography,
  useMediaQuery,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import React from "react";
import CatchPopup from "../../components/CatchPopup";
import { DetailNavbar } from "../../components/Navbar";
import { theme } from "../../global";
import { getPokeDetail } from "../../services/pokeapi";
import { storePokemon } from "../../services/pokedexapi";
import { Context } from "../../services/Store";
import { useStyles } from "./styles";

function PokeDetailPage(props) {
  const pokeid = props.match.params.pokeid;
  const classes = useStyles();
  const { state, dispatch } = React.useContext(Context);
  const [pokedata, setData] = React.useState({
    loading: true,
    data: null,
  });
  const isMobile = useMediaQuery(theme.breakpoints.down("md"), {
    defaultMatches: true,
  });

  React.useEffect(() => {
    getPokeDetail((res) => {
      console.log(res);
      setData((s) => ({
        ...s,
        data: res,
        loading: false,
      }));
    }, pokeid);
  }, [pokeid]);

  const getOwned = () => {
    const temp = state.countEach.filter(
      (item) => item.name === pokedata.data.name
    );
    return temp.length > 0 ? temp[0].count : 0;
  };

  const catchPokemon = (nickname) => {
    storePokemon(
      (res) => {
        dispatch({ type: "ADD_POKEMON", payload: res });
      },
      {
        url: `https://pokeapi.co/api/v2/pokemon/${pokeid}`,
        name: pokedata.data.name,
        nickname: nickname,
        pokeid: pokedata.data.id,
        imgurl: pokedata.data.sprites.front_default
          ? pokedata.data.sprites.front_default
          : `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/0.png`,
      }
    );
  };

  return (
    <React.Fragment>
      <DetailNavbar
        pokemonName={
          pokedata.loading ? "" : pokedata.data.name.replace(/-/g, " ")
        }
      />
      {pokedata.loading ? (
        <Typography id="loading"> loading...</Typography>
      ) : (
        <Grid
          container
          className={classes.root}
          alignContent="center"
          direction="column"
          spacing={2}
        >
          {!isMobile ? (
            <Grid item xs={10}>
              <Typography variant="h3" style={{ textTransform: "capitalize" }}>
                {pokedata.data.name.replace(/-/g, " ")}
              </Typography>
            </Grid>
          ) : null}
          <Grid item xs={10}>
            <Alert
              id="detail-owned-alert"
              icon={false}
              severity="success"
              color={getOwned() < 1 ? "info" : "success"}
            >
              {getOwned() < 1
                ? `You do not own this pokemon.`
                : `You own ${getOwned()} of this pokemon!`}
            </Alert>
          </Grid>
          <Grid item xs={10} className={classes.imageContainer}>
            <img
              className={classes.image}
              alt={pokeid}
              src={
                pokedata.data.sprites.front_default
                  ? pokedata.data.sprites.front_default
                  : `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/0.png`
              }
            />
          </Grid>
          <Grid item xs={10}>
            <Typography style={{ width: "100%" }}>Types</Typography>
          </Grid>
          <Grid item xs={10}>
            <Grid container spacing={1}>
              {pokedata.data.types.map((item, idx) => (
                <Grid item key={idx}>
                  <Chip label={item.type.name} />
                </Grid>
              ))}
            </Grid>
          </Grid>
          <Grid item xs={10}>
            <Typography>Moves</Typography>
          </Grid>
          <Grid item xs={10}>
            <Grid
              container
              spacing={1}
              component={(e) => (
                <Paper
                  {...e}
                  variant="outlined"
                  className={`${classes.movesContainer} ${e.className}`}
                />
              )}
            >
              {pokedata.data.moves.map((item, idx) => (
                <Grid item key={idx}>
                  <Chip label={item.move.name} />
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      )}
      <AppBar className={classes.appbar} color="secondary">
        <Toolbar className={classes.spaceEven}>
          <CatchPopup
            disabled={state.loading}
            className={classes.catchButton}
            onCaught={catchPokemon}
            type={pokedata.loading ? "" : pokedata.data.name.replace(/-/g, " ")}
          />
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}
export default PokeDetailPage;
