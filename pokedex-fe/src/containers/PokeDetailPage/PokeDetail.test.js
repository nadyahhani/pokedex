import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Store, { Context } from "../../services/Store";
const { screen } = require("@testing-library/react");
const { default: axios } = require("axios");
const { mount, configure, shallow } = require("enzyme");
const { render } = require("react-dom");
const { default: PokeDetailPage } = require(".");

configure({ adapter: new Adapter() });
describe("displaying pokemon detail", () => {
  const wrapper = mount(
    <Store>
      <PokeDetailPage match={{ params: { pokeid: "1" } }} />
    </Store>
  );
  it("should fetch and display a pokemon's data", () => {
    jest.mock("axios");
    expect(wrapper.find("#loading").exists()).toBe(true);

    axios.get = jest.fn().mockImplementationOnce(() => {
      return Promise.resolve({
        data: {
          id: 1,
          name: "bulbasaur",
          moves: [
            {
              move: {
                name: "razor-wind",
                url: "https://pokeapi.co/api/v2/move/13/",
              },
            },
          ],
          sprites: {
            front_default:
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png",
          },
          types: [
            {
              slot: 1,
              type: {
                name: "grass",
                url: "https://pokeapi.co/api/v2/type/12/",
              },
            },
          ],
        },
      });
    });
    expect(wrapper.find("#loading").exists()).toBe(false);

    expect(wrapper.find("h3").text()).toEqual("Bulbasaur");
  });
});
