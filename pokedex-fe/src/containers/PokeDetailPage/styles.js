import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(13),
    paddingBottom: theme.spacing(10),
  },
  image: { width: theme.spacing(20) },
  imageContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  movesContainer: {
    maxHeight: theme.spacing(30),
    overflowY: "scroll",
    padding: theme.spacing(1),
  },
  appbar: {
    top: "auto",
    bottom: 0,
  },
  spaceEven: {
    justifyContent: "space-evenly",
  },
  catchButton: {
    width: "100%",
    maxWidth: theme.spacing(50),
  },
}));
