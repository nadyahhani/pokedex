import {
  Badge,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Typography,
} from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import { Navbar } from "../../components/Navbar";
import { getPokeList } from "../../services/pokeapi";
import { Context } from "../../services/Store";
import { useStyles } from "./styles";

function PokeListPage(props) {
  const classes = useStyles();
  const history = useHistory();
  const { state } = useContext(Context);
  const [list, setList] = useState({
    loading: true,
    data: null,
    next: null,
    previous: null,
    page: 1,
    count: 1,
  });
  const page = props.match.params.page ? parseInt(props.match.params.page) : 1;

  const changePage = (e, pagenum) => {
    history.push(`/pokedex/${pagenum}`);
  };

  React.useEffect(() => {
    setList((s) => ({ ...s, loading: true }));
    getPokeList(
      (res) =>
        setList((s) => ({
          ...s,
          data: [...res.results],
          previous: res.previous,
          next: res.next,
          loading: false,
          count: Math.ceil(res.count / 30),
        })),
      page
    );
  }, [page]);

  return (
    <div>
      <Navbar />
      <Grid
        container
        className={classes.root}
        alignItems="center"
        direction="column"
        spacing={2}
      >
        <Grid item xs={10}>
          {list.loading || state.loading ? (
            <Typography>Loading...</Typography>
          ) : (
            <Grid container spacing={2} alignContent="center">
              {list.data.map((item, idx) => {
                const currentPokemon = item.url.split("/")[6];
                let imagesrc = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
                  currentPokemon >= 10091 ? 0 : currentPokemon
                }.png`;
                var isOwned = state.countEach.filter(function (i) {
                  return i.name === item.name;
                });
                return (
                  <Grid item xs={6} lg={2} key={`${idx}`}>
                    <Badge
                      invisible={isOwned.length < 1}
                      color="primary"
                      badgeContent={
                        isOwned.length > 0 ? isOwned[0].count : null
                      }
                    >
                      <Card>
                        <CardActionArea
                          onClick={() =>
                            history.push(`/detail/${currentPokemon}`)
                          }
                          id={`card-list-${item.name}`}
                        >
                          <CardMedia
                            component="img"
                            className={
                              isOwned.length < 1 ? classes.notOwned : null
                            }
                            alt={item.name}
                            title={item.name}
                            src={imagesrc}
                          />
                          <CardContent>
                            <Typography style={{ textTransform: "capitalize" }}>
                              {item.name.replace(/-/g, " ")}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </Badge>
                  </Grid>
                );
              })}
            </Grid>
          )}
        </Grid>
        <Grid item xs={10}>
          <Pagination onChange={changePage} page={page} count={list.count} />
        </Grid>
      </Grid>
    </div>
  );
}
export default PokeListPage;
