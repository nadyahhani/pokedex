import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(13),
    paddingBottom: theme.spacing(8),
  },
  notOwned: {
    opacity: 1,
  },
  
}));
