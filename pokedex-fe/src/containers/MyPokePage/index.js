import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  IconButton,
  Typography,
} from "@material-ui/core";
import { Clear } from "@material-ui/icons";
import { Alert } from "@material-ui/lab";
import React from "react";
import { useHistory } from "react-router-dom";
import { Navbar } from "../../components/Navbar";
import { deletePokemon } from "../../services/pokedexapi";
import { Context } from "../../services/Store";
import { useStyles } from "./styles";

function MyPokePage() {
  const classes = useStyles();
  const history = useHistory();
  const { state, dispatch } = React.useContext(Context);

  return (
    <div>
      <Navbar title="My Pokémons" />
      <Grid
        container
        className={classes.root}
        alignItems="center"
        direction="column"
        spacing={2}
      >
        <Grid item xs={10}>
          {state.loading ? (
            <Typography id="loading">Loading...</Typography>
          ) : (
            <Grid container spacing={2} alignContent="center">
              {state.pokemons.map((item, idx) => {
                return (
                  <Grid item xs={12} lg={6} key={`pokedex-${idx}`}>
                    <Card className={classes.cardRoot}>
                      <CardActionArea
                        onClick={() => history.push(`/detail/${item.pokeid}`)}
                        className={classes.cardAction}
                      >
                        <CardMedia
                          className={classes.pokemonImg}
                          component="img"
                          title={item.name}
                          alt={item.name}
                          src={item.imgurl}
                        />
                        <CardContent className={classes.centerVert}>
                          <Typography
                            style={{
                              fontWeight: "bold",
                              textTransform: "capitalize",
                            }}
                          >
                            {item.nickname}
                          </Typography>
                          <Typography style={{ textTransform: "capitalize" }}>
                            {item.name.replace(/-/g, " ")}
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                      <CardContent>
                        <IconButton
                          color="primary"
                          onClick={() =>
                            deletePokemon(() => {
                              dispatch({
                                type: "DELETE_POKEMON",
                                payload: { _id: item._id, name: item.name },
                              });
                            }, item._id)
                          }
                        >
                          <Clear />
                        </IconButton>
                      </CardContent>
                    </Card>
                  </Grid>
                );
              })}
              {state.pokemons.length < 1 && !state.loading ? (
                <Grid item xs={12}>
                  <Alert severity="error" icon={false} id="no-pokemons-alert">
                    You don't have any pokemons yet :(
                  </Alert>
                </Grid>
              ) : null}
            </Grid>
          )}
        </Grid>
      </Grid>
    </div>
  );
}
export default MyPokePage;
