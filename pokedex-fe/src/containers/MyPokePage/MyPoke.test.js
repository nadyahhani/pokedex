import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Store from "../../services/Store";
const { default: axios } = require("axios");
const { mount, configure } = require("enzyme");
const { default: MyPokePage } = require(".");

configure({ adapter: new Adapter() });
describe("displaying pokemon detail", () => {
  jest.mock("axios");

  const wrapper = mount(
    <Store>
      <MyPokePage />
    </Store>
  );
  it("should fetch and display pokemons", () => {
    expect(wrapper.find("#loading").exists()).toBe(true);
    axios.get = jest.fn().mockResolvedValueOnce({
      data: {
        countTotal: 1,
        countEach: [
          {
            name: "bulbasaur",
            count: 1,
          },
        ],
        pokemons: [
          {
            _id: "5fa83872fa5857033ec5fee7",
            name: "bulbasaur",
            nickname: "boulby",
            url: "https://pokeapi.co/api/v2/pokemon/1/",
            imgurl:
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png",
            __v: 0,
          },
        ],
      },
    });
    expect(wrapper.find("#loading").exists()).toBe(false);
  });
});
