import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(13),
    paddingBottom: theme.spacing(8),
    [theme.breakpoints.down("md")]: {
      paddingTop: theme.spacing(10),
    },
  },
  cardRoot: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  cardAction: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  pokemonImg: {
    width: theme.spacing(18),
    [theme.breakpoints.down("md")]: { width: theme.spacing(10) },
  },
  centerVert: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
}));
