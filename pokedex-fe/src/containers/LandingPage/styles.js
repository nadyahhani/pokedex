import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    flexGrow: 1,
  },
  cardcontainer: {
    display: "flex",
    alignItems: "center",
  },
  card: {
    [theme.breakpoints.down("md")]: {
      minHeight: "35vh",
    },
    minHeight: theme.spacing(45),
    display: "flex",
    direction: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  myPokeCard: {
    position: "absolute",
    opacity: ".35",
    maxHeight: "100%",
  },
  pokedexCard: {
    position: "absolute",
    opacity: ".35",
  },
  cardText: {
    zIndex: 2,
    position: "absolute",
  },
}));
