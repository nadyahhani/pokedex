import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Typography,
} from "@material-ui/core";
import React from "react";
import { LandingNavbar } from "../../components/Navbar";
import { useStyles } from "./styles";
import pokeball from "../../images/pokeball.png";
import { useHistory } from "react-router-dom";

export default function LandingPage() {
  const classes = useStyles();
  const history = useHistory();

  return (
    <div>
      <LandingNavbar />
      <Grid
        container
        className={classes.root}
        alignContent="center"
        direction="column"
        spacing={2}
      >
        <Grid item xs={10} className={classes.cardcontainer}>
          <Grid container direction="row" spacing={2} >
            <Grid item xs={12} lg={6}>
              <Card>
                <CardActionArea
                  onClick={() => history.push("/pokedex/1")}
                  className={classes.card}
                >
                  <CardMedia
                    className={classes.pokedexCard}
                    component="img"
                    src="https://data.whicdn.com/images/71286714/original.gif"
                    alt="pokedex"
                    title="pokedex"
                  />
                  <CardContent className={classes.card}>
                    <Typography variant="h3" className={classes.cardText}>
                      Pokédex
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item xs={12} lg={6}>
              <Card>
                <CardActionArea
                  onClick={() => history.push("/my-pokemons")}
                  className={classes.card}
                >
                  <CardMedia
                    className={classes.myPokeCard}
                    component="img"
                    src={pokeball}
                    alt="pokedex"
                    title="pokedex"
                  />
                  <CardContent className={classes.card}>
                    <Typography variant="h3" className={classes.cardText}>
                      My Pokémons
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
