import React from "react";
import {
  Badge,
  Button,
  IconButton,
  Toolbar,
  Typography,
  useMediaQuery,
} from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import { makeStyles } from "@material-ui/core/styles";
import { ArrowBackIos } from "@material-ui/icons";
import { theme } from "../../global";
import { useHistory } from "react-router-dom";
import { Context } from "../../services/Store";

const useStyles = makeStyles(() => ({
  appbar: {
    [theme.breakpoints.down("md")]: {
      top: "auto",
      bottom: 0,
    },
  },
  spaceEven: {
    justifyContent: "space-evenly",
  },
  spaceBetween: {
    justifyContent: "space-between",
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
  },
}));

function BaseNavbar(props) {
  const classes = useStyles();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"), {
    defaultMatches: true,
  });

  const path = window.location.pathname;
  const history = useHistory();
  const { state } = React.useContext(Context);

  return (
    <React.Fragment>
      <AppBar className={classes.appbar} color="primary">
        <Toolbar className={classes.spaceEven}>
          <Button
            id="navbar-home-button"
            color="secondary"
            onClick={() => history.push("/")}
            style={path === "/" ? { fontWeight: "700" } : null}
          >
            Home
          </Button>
          <Button
            id="navbar-pokedex-button"
            color="secondary"
            onClick={() => history.push("/pokedex/1")}
            style={path.includes("pokedex") ? { fontWeight: "700" } : null}
          >
            Pokédex
          </Button>
          <Badge
            invisible={!state.countTotal}
            color="secondary"
            badgeContent={state.countTotal}
          >
            <Button
              id="navbar-mypoke-button"
              color="secondary"
              onClick={() => history.push("/my-pokemons")}
              style={path === "/my-pokemons" ? { fontWeight: "700" } : null}
            >
              My Pokémons
            </Button>
          </Badge>
        </Toolbar>
      </AppBar>

      {!props.hideTop && isMobile ? (
        <AppBar className={classes.appbarTopMobile} color="primary">
          <Toolbar className={props.back ? null : classes.spaceBetween}>
            {props.back ? (
              <IconButton onClick={() => history.goBack()} color="secondary">
                <ArrowBackIos />
              </IconButton>
            ) : null}
            <Typography style={{ textTransform: "capitalize" }}>
              {props.pagetitle}
            </Typography>
          </Toolbar>
        </AppBar>
      ) : null}
    </React.Fragment>
  );
}

export function Navbar(props) {
  return <BaseNavbar pagetitle={props.title ? props.title : "Pokédex"} />;
}

export function DetailNavbar(props) {
  return <BaseNavbar pagetitle={props.pokemonName} back={"/pokedex/1"} />;
}

export function LandingNavbar(props) {
  return <BaseNavbar pagetitle={props.pokemonName} hideTop />;
}
