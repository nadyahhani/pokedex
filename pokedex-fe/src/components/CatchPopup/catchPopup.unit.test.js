const { configure, mount } = require("enzyme");
import { act } from "@testing-library/react";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import CatchPopup from ".";

configure({ adapter: new Adapter() });
test("pressing catch button", () => {
  const caughtfn = jest.fn();
  const wrapper = mount(<CatchPopup type="bulbasaur" onCaught={caughtfn} />);

  act(() => {
    wrapper.find("#catch-button").at(0).simulate("click");
    expect(wrapper.find(".popup").exists()).toBe(true);
  });
});
