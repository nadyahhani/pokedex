import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Typography,
} from "@material-ui/core";
import React from "react";

function PopupSuccess(props) {
  return (
    <Dialog
      className="popup"
      id="success-popup"
      open={props.open}
      onClose={() => {}}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle style={{ textTransform: "capitalize" }}>
        You caught {props.type}!
      </DialogTitle>
      <DialogContent>
        <DialogContentText>Give it a name!</DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label=""
          placeholder={props.type}
          variant="filled"
          fullWidth
          onChange={(e) => props.onType(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button
          key="success-popup-button"
          variant="contained"
          onClick={() => {
            props.onCaught();
          }}
          color="primary"
        >
          Save to My Pokémons
        </Button>
      </DialogActions>
    </Dialog>
  );
}

function PopupFailed(props) {
  return (
    <Dialog
      className="popup"
      id="failed-popup"
      open={props.open}
      onClose={props.onClose}
    >
      <DialogTitle style={{ textTransform: "capitalize" }}>
        {props.type} Fled!
      </DialogTitle>
      <DialogContent>
        <Typography>Better Luck Next Time.</Typography>
      </DialogContent>
      <DialogActions>
        <Button variant="contained" onClick={props.onClose} color="primary">
          Okay...
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default function CatchPopup(props) {
  const [success, setSuccess] = React.useState(null);
  const [nickname, setNickname] = React.useState("");

  const catchPokemon = () => {
    if (Math.random() < 0.5) {
      setNickname(props.type);
      setSuccess("true");
    } else {
      setSuccess("false");
    }
  };
  return (
    <div className={props.className}>
      <PopupSuccess
        open={success === "true"}
        type={props.type}
        onType={(val) => setNickname(val)}
        onCaught={() => {
          setSuccess(null);
          props.onCaught(nickname);
        }}
      />
      <PopupFailed
        open={success === "false"}
        onClose={() => setSuccess(null)}
        type={props.type}
      />
      <Button
        key="catch-button"
        id="catch-button"
        fullWidth
        variant="contained"
        color="primary"
        onClick={catchPokemon}
        disabled={props.disabled}
      >
        {props.disabled ? "Already Caught" : "Catch"}
      </Button>
    </div>
  );
}
